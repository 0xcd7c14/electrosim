#include "lib/imgui.h"
#include "lib/imgui-SFML.h"

#include "lib/json.hpp"

#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/System/Clock.hpp>
#include <SFML/Window/Event.hpp>
#include <SFML/Graphics.hpp>

#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include <math.h>

using json = nlohmann::json;

const int WIDTH = 800;
const int HEIGHT = 800;

const int RADIUS = 4;

const long double E0 = 8.854 * pow(10, -12);
const long double K = 9 * pow(10, 9);  // K = 1/(4*pi*E0)

float gravity = 0.0f;
float relperm = 1.0f;

const long double DEG2RAD = 0.012453f;

long double delta_t = 0.001;
long double t = 0;

sf::Vertex xaxis[] = {
	sf::Vertex(sf::Vector2f(0.f, (HEIGHT)/2), sf::Color::Black),
	sf::Vertex(sf::Vector2f(WIDTH, (HEIGHT)/2), sf::Color::Black)
};

sf::Vertex yaxis[] = {
	sf::Vertex(sf::Vector2f((WIDTH/2), 0.f), sf::Color::Black),
	sf::Vertex(sf::Vector2f((WIDTH/2), (HEIGHT)), sf::Color::Black)
};

struct Charge {
	double value;
	double mass;
	bool fixed_x;
	bool fixed_y;
	long double x;
	long double y;
	long double vx;
	long double vy;
};

struct Force {
	long double x;
	long double y;
};

enum Mode {
	Fresh,
	AddPositiveCharge,
	AddNegativeCharge,
	SelectCharge,
	ChargeSelected,
	SimulationRunning,
	SimulationPaused,
	LoadFromFile,
	SaveToFile
};

int signum(int x) {
	if (x < 0) {
		return -1;
	}
	else if (x > 0) {
		return 1;
	}
	return 0;
}

Force calculate_force(std::vector<Charge> &charges, int index) {
	Force netforce;
	netforce.x = 0;
	netforce.y = 0;
	if (charges[index].fixed_x && charges[index].fixed_y) {
		return netforce;
	}
	long double K_prime = K/relperm;
	/*
		E_r = E/E0
		K' = 1/(4*pi*E)
		K' = 1/(4*pi*E_r*E0)
		K' = K/E_r
		[relperm = E_r]
	*/
	for (int i = 0; i < charges.size(); ++i) {
		if (i != index) {
			// F = K'*q1*q2/r^2
			long double r2 = pow((charges[i].x - charges[index].x), 2) + pow((charges[i].y - charges[index].y), 2);
			long double f = ((K_prime*charges[i].value*charges[index].value*pow(10, -4)))/r2;
			// long double theta = atan2((charges[index].y - charges[i].y), (charges[index].x - charges[i].x));
			// netforce.x += f*cos(theta);
			// netforce.y += f*sin(theta);
			netforce.x += f*(charges[index].x - charges[i].x)/sqrt(r2);
			netforce.y += f*(charges[index].y - charges[i].y)/sqrt(r2);
		}
	}
	netforce.y += gravity;
	if (charges[index].fixed_x) {
		netforce.y = 0;
	}
	if (charges[index].fixed_y) {
		netforce.x = 0;
	}
	return netforce;
}

std::string serialize(std::vector<Charge> &charges) {
	json j;
	j["gravity"] = gravity;
	j["relpermittivity"] = relperm;
	j["charges"] = json::array();
	for (auto ch : charges) {
		j["charges"].push_back({
			{"charge", ch.value},
			{"mass", ch.mass},
			{"fixed_x", ch.fixed_x},
			{"fixed_y", ch.fixed_y},
			{"x", ch.x - (WIDTH/2)},
			{"y", (HEIGHT/2) - ch.y},
			{"vx", ch.vx},
			{"vy", ch.vy}
		});
	}
	return j.dump(/* indent */ 4);
}

std::vector<Charge> deserialize(std::string data) {
	std::vector<Charge> new_charges;
	json j = json::parse(data);
	gravity = j["gravity"].get<long double>();
	relperm = j["relpermittivity"].get<long double>();
	for (auto ch : j["charges"]) {
		new_charges.push_back({
			ch["charge"].get<long double>(),
			ch["mass"].get<long double>(),
			ch["fixed_x"].get<bool>(),
			ch["fixed_y"].get<bool>(),
			ch["x"].get<long double>() + (WIDTH/2),
			(HEIGHT/2) - ch["y"].get<long double>(),
			ch["vx"].get<long double>(),
			ch["vy"].get<long double>()
		});
	}
	return new_charges;
}

std::string read_file(std::string filename) {
	std::ifstream t(filename);
	if (! t) {
		return "";
	}
	std::stringstream buffer;
	buffer << t.rdbuf();
	return buffer.str();
}

void write_file(std::string filename, std::string data) {
	std::ofstream out(filename);
	if (! out) {
		return;
	}
	out << data;
}

int charge_exists_at(std::vector<Charge> &charges, int x, int y) {
	int i = 0;
	sf::CircleShape circ;
	circ.setRadius(RADIUS);
	for (auto ch : charges) {
		circ.setPosition((ch.x)-(RADIUS), (ch.y)-(RADIUS));
		if (circ.getGlobalBounds().contains(x, y)) {
			return i;
		}
		i++;
	}
	return -1;
}

int main() {
	
	sf::RenderWindow win(sf::VideoMode(WIDTH, HEIGHT), "electrosim");
	win.setVerticalSyncEnabled(true);
	ImGui::SFML::Init(win);

	ImGuiIO& io = ImGui::GetIO();
	io.IniFilename = NULL;
	io.LogFilename = NULL;

	int selected_charge = -1;
	std::vector<Charge> charges;

	sf::CircleShape circle;
	circle.setRadius(RADIUS);
	circle.setOutlineColor(sf::Color::Black);

	bool running = false;
	bool load_from_file = false;
	bool save_to_file = false;

	char load_filepath[4096] = "";
	char save_filepath[4096] = "";

	win.resetGLStates();
	sf::Clock deltaClock;
	while (win.isOpen()) {
		sf::Event event;
		while (win.pollEvent(event)) {
			ImGui::SFML::ProcessEvent(event);
			if (event.type == sf::Event::Closed) {
				win.close();
			}
			if (event.type == sf::Event::MouseButtonPressed) {
				int mousex = event.mouseButton.x;
				int mousey = event.mouseButton.y;
				if (sf::Keyboard::isKeyPressed(sf::Keyboard::LControl)) {
					charges.push_back({+1.0, 1.0, false, false, mousex, mousey, 0, 0});
					selected_charge = charges.size() - 1;
				}
				if (sf::Keyboard::isKeyPressed(sf::Keyboard::LShift)) {
					selected_charge = charge_exists_at(charges, mousex, mousey);
				}
			}
			if (event.type == sf::Event::KeyPressed) {
				sf::Vector2i mousepos = sf::Mouse::getPosition(win);
				if (event.key.code == sf::Keyboard::Space) {
					running = !running;
				}
				if (event.key.code == sf::Keyboard::Up) {
					sf::Mouse::setPosition({mousepos.x, mousepos.y - 1}, win);
				}
				if (event.key.code == sf::Keyboard::Down) {
					sf::Mouse::setPosition({mousepos.x, mousepos.y + 1}, win);
				}
				if (event.key.code == sf::Keyboard::Left) {
					sf::Mouse::setPosition({mousepos.x - 1, mousepos.y}, win);
				}
				if (event.key.code == sf::Keyboard::Right) {
					sf::Mouse::setPosition({mousepos.x + 1, mousepos.y}, win);
				}

			}
		}

		ImGui::SFML::Update(win, deltaClock.restart());

		sf::Vector2i mousepos = sf::Mouse::getPosition(win);

		ImVec2 window_pos = ImVec2(WIDTH-100, HEIGHT-50);
		ImVec2 window_pos_pivot = ImVec2(0.0f, 0.0f);
		ImGui::SetNextWindowPos(window_pos, ImGuiCond_Always, window_pos_pivot);
		ImGui::SetNextWindowBgAlpha(0.4f);
		ImGuiWindowFlags window_flags = ImGuiWindowFlags_NoDecoration | ImGuiWindowFlags_AlwaysAutoResize | 
			ImGuiWindowFlags_NoSavedSettings | ImGuiWindowFlags_NoFocusOnAppearing | ImGuiWindowFlags_NoNav | 
			ImGuiWindowFlags_NoMove;
		ImGui::Begin("overlay", NULL, window_flags);
			ImGui::Text(std::string("("+std::to_string(mousepos.x-(WIDTH/2))+", "+
				std::to_string((HEIGHT/2)-mousepos.y)+")").data());
			ImGui::Text(running ? "running..." : "paused");
		ImGui::End();

		ImGui::SetNextWindowPos(ImVec2(50.0f, 50.0f), ImGuiCond_Once, ImVec2(0.0f, 0.0f));
		ImGui::Begin("control");
			ImGui::PushItemWidth(100);
			ImGui::InputFloat("gravity (g)", &gravity, /* step */ 1, /* step_fast */ 1, /* display format */ "%.2f");
			ImGui::PushItemWidth(100);
			ImGui::InputFloat("rel. permittivity", &relperm, /* step */ 1, /* step_fast */ 1, /* display format */ "%.2f");
			if (ImGui::Button("load from file")) {
				load_from_file = true;
			}
			ImGui::SameLine();
			if (ImGui::Button("save to file")) {
				save_to_file = true;
			}
			ImGui::Text("ctrl-click to add charge");
			ImGui::Text("shift-click to edit charge");
			ImGui::Text("space to start/pause simulation");
		ImGui::End();

		if (load_from_file) {
			ImGui::SetNextWindowPos(ImVec2(50.0f, 220.0f), ImGuiCond_Once, ImVec2(0.0f, 0.0f));
			ImGui::Begin("load from file", NULL,
				ImGuiWindowFlags_NoSavedSettings | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_AlwaysAutoResize);
			ImGui::InputText("file path", load_filepath, IM_ARRAYSIZE(load_filepath));
			if (ImGui::Button("load")) {
				std::string loaded_file = read_file(std::string(load_filepath));
				if (loaded_file != "") {
					std::vector<Charge> new_charges = deserialize(read_file(std::string(load_filepath)));
					charges.clear();
					charges.reserve(charges.size() + new_charges.size());
					charges.insert(charges.end(), new_charges.begin(), new_charges.end());
				}
				load_from_file = false;
			}
			ImGui::End();
		}

		if (save_to_file) {
			ImGui::SetNextWindowPos(ImVec2(50.0f, 220.0f), ImGuiCond_Once, ImVec2(0.0f, 0.0f));
			ImGui::Begin("save to file", NULL,
				ImGuiWindowFlags_NoSavedSettings | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_AlwaysAutoResize);
			ImGui::InputText("file path", save_filepath, IM_ARRAYSIZE(save_filepath));
			if (ImGui::Button("save")) {
				write_file(std::string(save_filepath), serialize(charges));
				save_to_file = false;
			}
			ImGui::End();
		}

		if (selected_charge != -1) {
			ImGui::SetNextWindowPos(ImVec2(50.0f, 220.0f), ImGuiCond_Once, ImVec2(0.0f, 0.0f));
			ImGui::Begin(std::string("edit charge #" + std::to_string(selected_charge+1)).data(), NULL,
				ImGuiWindowFlags_NoSavedSettings | ImGuiWindowFlags_NoResize);
			ImGui::InputDouble("charge", &(charges[selected_charge].value), 1, 1, "%f");
			ImGui::InputDouble("mass", &(charges[selected_charge].mass), 1, 1, "%f");
			ImGui::Text(std::string("x  = " + std::to_string(charges[selected_charge].x - (WIDTH/2))).data());
			ImGui::Text(std::string("y  = " + std::to_string((HEIGHT/2) - charges[selected_charge].y)).data());
			ImGui::Text(std::string("vx = " + std::to_string(charges[selected_charge].vx)).data());
			ImGui::Text(std::string("vy = " + std::to_string(charges[selected_charge].vy)).data());
			ImGui::Checkbox("no vertical movement", &charges[selected_charge].fixed_x);
			ImGui::Checkbox("no horizontal movement", &charges[selected_charge].fixed_y);

			ImGui::PushStyleColor(ImGuiCol_Button, ImVec4{1.0f, 0.3f, 0.4f, 1.0f});
			if (ImGui::Button("delete")) {
				charges.erase(charges.begin() + selected_charge);
				selected_charge = -1;
			}
			ImGui::PopStyleColor();
			ImGui::SameLine(ImGui::GetWindowWidth() - 40);
			if (ImGui::Button("done")) {
				selected_charge = -1;
			}
			ImGui::End();
		}

		win.clear(sf::Color::White);
		
		win.draw(xaxis, 2, sf::Lines);
		win.draw(yaxis, 2, sf::Lines);

		for (int j = 0; j < 100; ++j) {
			std::vector<Charge> charges_tmp (charges);
			for (int i = 0; i < charges.size(); ++i) {
				Charge * ch = &charges.at(i);
				if (running) {
					Force f = calculate_force(charges_tmp, i);			
					// semi-implicit euler
					ch->vx = ch->vx + f.x * delta_t;
					ch->x = ch->x + ch->vx * delta_t;
					ch->vy = ch->vy + f.y * delta_t;
					ch->y = ch->y + ch->vy * delta_t;
				}
			}
		}
		for (int i = 0; i < charges.size(); ++i) {
			Charge * ch = &charges.at(i);
			circle.setPosition(round(ch->x)-(RADIUS), round(ch->y)-(RADIUS));
			if (ch->value > 0) {
				circle.setFillColor(sf::Color::Red);
			} else {
				circle.setFillColor(sf::Color::Blue);
			}
			if (selected_charge == i) {
				circle.setOutlineThickness(-1.0f);
			} else {
				circle.setOutlineThickness(0);
			}
			win.draw(circle);
		}

		ImGui::SFML::Render(win);
		
		win.display();
	}

	ImGui::SFML::Shutdown();

}