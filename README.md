# electrosim
### simulate electrostatics and electrodynamics

![example: simple harmonic motion](.img/linear-shm.gif)

**electrosim** can simulate 2D systems using gravity and charges.

### features

  - adjustable gravity
  - adjustable permittivity
  - can save and load simulations from file
  - note: very complex simulations will run fine for a few minutes then they may blow up (due to limitations in floating point accuracy)

![electrosim](.img/electrosim.png)

### install

build requirements: SFML

```console
# build
$ sh build.sh

# run
$ ./electrosim
```

### examples

![example: linear-shm](.img/linear-shm.gif)

[simulation file](examples/linear-shm.json)

![example: swinging-shm](.img/swinging-shm.gif)

[simulation file](examples/swinging-shm.json)

### save file format

**electrosim** can save and load simulations in this JSON format:

```
{
	"gravity": <float, value of g in m/s^2>,
	"relpermittivity": <float, relative permittivity>,
	"charges": [
		{
			"charge": <float, charge in 10^(-2) coulomb>,
			"mass": <float, mass in kg>,
			"x": <float, x coordinate>,
			"y": <float, y coordinate>,
			"vx": <float, velocity in x direction>,
			"vy": <float, velocity in y direction>,
			"fixed_x": <bool, whether charge is constrained to x-axis (i.e. no vertical movement)>,
			"fixed_y": <bool, whether charge is constrained to y-axis (i.e. no horizontal movement)>
		},
		...
	]
}
```